﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolveTheProblem : MonoBehaviour {

    public float TheDistance;
    public GameObject ActionDisplay; //Push [`] Key
    public GameObject ActionText; //Solve the coding problem

    void Update()
    {
        TheDistance = PlayerCasting.DistanceFromTarget;
    }

    void OnMouseOver()
    {
        if (TheDistance <= 10) //책장과 player 사이의 거리
        {
            //ExtraCross.SetActive(true);
            ActionDisplay.SetActive(true); //Push [`] Key 화면 출력
            ActionText.SetActive(true); //Solve the coding problem 출력
        }

        //문제 풀기 위해 [`] 버튼 누를 시
        if (Input.GetButtonDown("q")) //Action 
        {
            if (TheDistance <= 3) //play와 door사이의 거리가 2 이하면
            {
                this.GetComponent<BoxCollider>().enabled = false; //충돌 off
                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);
               
            }
        }
    }

    void OnMouseExit() //mouse가 책장에서 벗어나면
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
