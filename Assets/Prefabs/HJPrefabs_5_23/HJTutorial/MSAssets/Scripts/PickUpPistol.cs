﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpPistol : MonoBehaviour {

    public float TheDistance;
    public GameObject ActionDisplay; //[E] 출력
    public GameObject ActionText; //open the door 출력
    public GameObject FakePistol;
    public GameObject RealPistol;
    public GameObject GuideArrow;
    public GameObject ExtraCross;
    public GameObject TheJumpTrigger;

    void Update()
    {
        TheDistance = PlayerCasting.DistanceFromTarget;
    }

    void OnMouseOver()
    {
        if (TheDistance <= 2)
        {
            ExtraCross.SetActive(true);
            ActionText.GetComponent<Text>().text = "Pick Up Pistol";
            ActionDisplay.SetActive(true); //[E] 화면 출력
            ActionText.SetActive(true); //the door open 출력
        }
        if (Input.GetButtonDown("Action")) //Action 
        {
            if (TheDistance <= 2) //play와 door사이의 거리가 2 이하면
            {
                this.GetComponent<BoxCollider>().enabled = false; //충돌 off
                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);
                FakePistol.SetActive(false);
                RealPistol.SetActive(true);
                ExtraCross.SetActive(false);
                GuideArrow.SetActive(false);
                TheJumpTrigger.SetActive(true);
            }
        }
    }

    void OnMouseExit() //mouse가 door에서 벗어나면
    {
        ExtraCross.SetActive(false);
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
