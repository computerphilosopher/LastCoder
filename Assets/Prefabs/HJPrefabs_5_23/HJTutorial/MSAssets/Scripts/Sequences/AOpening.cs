﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class AOpening : MonoBehaviour {

    public GameObject Player;
    public GameObject FadeScreenIn;
    public GameObject TextBox;
	void Start () {
        Player.GetComponent<ThirdPersonUserControl>().enabled = false;
        StartCoroutine(ScenePlayer());

	}
    IEnumerator ScenePlayer()
    {
        yield return new WaitForSeconds(2f);
        FadeScreenIn.SetActive(false);
        TextBox.GetComponent<Text>().text = "Access the bookshelf";
        yield return new WaitForSeconds(2f);
        TextBox.GetComponent<Text>().text = " ";
        Player.GetComponent<ThirdPersonUserControl>().enabled = true;
    }
	
}
