﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class BFirstTrigger : MonoBehaviour {

    public GameObject TextBox;

    float timer;
    bool display = false;

    void Update()
    {
        timer += Time.deltaTime;
        if (display && timer > 2.0f)
        {
            TextBox.GetComponent<Text>().text = " ";
            display = false;
        }
    }

    private void OnTriggerEnter()
    {
        timer = 0f;
        display = true;
        TextBox.GetComponent<Text>().text = "Zombie Spawn! Kill the Zombie and get the score";
     
    }
   
}
