﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class starting : MonoBehaviour
{
    public Canvas ErrorCanvas; // 로그인 버튼 눌렀을 때 실패했을 때 떠야 할 캔버스 
    public Canvas RankingCanvas; // 상위권 점수 보여줄 캔버스

    public Button LoginButton; // 로그인 버튼 
    public Button CreateButton; // 회원가입 버튼
    public Button ScoreButton; // 점수 조회 버튼
    public Button RtBtn;
    //    public Button retBtn; // id 비밀번호 틀렸을 때 돌아가기 버튼        
    // Btn 안 쓰고 그냥 텍스트로 클릭하기
    public Text id1, id2, id3, id4, id5;
    public Text scroe1, scroe2, scroe3, scroe4, scroe5;

    public Button ClrBtn;
    public InputField IDInputField; // id 입력용
    public InputField PWInputField; // pw 입력용 
//    public static string IDInputField.text;
    //  public InputField NIDInputField; // 회원가입용 id
    //  public InputField NPassInputField; // 회원가입용 pw
    //  public InputField RNPassInputField; // 회원가입용 확인 pw
    public static string a;
    public  string b;
    public  string c;
    public  string LoginUrl;
    public  string CheckUrl;
    //    public string temp1, temp2;
    // 여기 스크립트에다가 뭔가를 많이 해줘야 될 듯...?
    public static string temiid;
 //   temiid = IDInputField.text;
    // Use this for initialization
    void Start()
    {
        ErrorCanvas = ErrorCanvas.GetComponent<Canvas>();
        LoginButton = LoginButton.GetComponent<Button>();
        CreateButton = CreateButton.GetComponent<Button>();
        ScoreButton = ScoreButton.GetComponent<Button>();
        RankingCanvas = RankingCanvas.GetComponent<Canvas>();
        ErrorCanvas.enabled = false;
        RankingCanvas.enabled = false;
        //        LoginUrl = "http://localhost:8000/register/net.jsp";
        //        CheckUrl = "http://localhost:8000/register/Check.jsp";
        LoginUrl = "http://35.231.97.240:8080/net.jsp";
        CheckUrl = "http://35.231.97.240:8080/Check.jsp";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Loginclick()
    {
        StartCoroutine(Loginco());

    }
    IEnumerator Loginco()
    {
        temiid = IDInputField.text;
        WWWForm form = new WWWForm();
        form.AddField("id", IDInputField.text);
        form.AddField("pw", PWInputField.text);
        UnityWebRequest www = UnityWebRequest.Post(LoginUrl, form);

        // 밑에 부분 추가했음 
        www.downloadHandler = new DownloadHandlerBuffer();
        //SceneManager.LoadScene(1);
        //Canvas 띄우는 거 아닌가 ?
        // Debug.Log(www.downloadHandler.text);
        // 조건문 던져주고 
        // 만약 login 버튼을 눌렀을 때 성공했을 땐 
        // 이어하기, 처음부터 Scnen으로 
        // if() 실패했을 경우엔 error 창 띄우기 
        //
        // logBtn.enabled = false;
        // creBtn.enabled = false;
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            a = www.downloadHandler.text;
            Debug.Log(a);
            c = "kit3";
     //       Debug.Log(a.IndexOf(c) + "결과값확인");
            Debug.Log("Form upload complete!");
            byte[] result = www.downloadHandler.data;
            b = a.IndexOf(c).ToString();
            Debug.Log(b);
            if (b.Equals("-1")) // 아이디가 없다면 
            {
                ErrorCanvas.enabled = true;
                LoginButton.enabled = false;
                CreateButton.enabled = false;
                
            }
            else // 아이디가 존재한다면 
            {
                SceneManager.LoadScene(2);
            }
        }

        Debug.Log(IDInputField.text);
        Debug.Log(PWInputField.text);

        // yield return webRequest.SendWebRequest();

    }
    public void RetPress()
    {
        ErrorCanvas.enabled = false;
        LoginButton.enabled = true;
        CreateButton.enabled = true;
    }

    public void CreClick()
    {
        SceneManager.LoadScene(1);
        //        Application.LoadLevel(1);// 회원가입 버튼을 누르면 회원가입 씬으로 넘어간다.
        //        StartCoroutine(Createco()); // 
    }

    public void CheckClick()
    {
        StartCoroutine(Checking());
    }
    IEnumerator Checking()
    {
        
        WWWForm form = new WWWForm();
        UnityWebRequest www = UnityWebRequest.Post(CheckUrl, form);
        yield return www.SendWebRequest();
        string total = www.downloadHandler.text;
        string bb = total.Substring(255, 140);
        char d = ' ';
        string[] status = bb.Split(d);
        Debug.Log(status[1]);
        id1.text = status[1];
        scroe1.text = status[3];
        id2.text = status[5];
        scroe2.text = status[7];
        id3.text = status[9];
        scroe3.text = status[11];
        id4.text = status[13];
        scroe4.text = status[15];
        id5.text = status[17];
        scroe5.text = status[19];
        RankingCanvas.enabled = true;
        //  Debug.Log(status[1]);
        //  Debug.Log(status[3]);
        //  Debug.Log(status[5]);
        //  Debug.Log(status[7]);
//        Debug.Log(total.IndexOf("kitae"));
//        Debug.Log(bb);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }

        else { 
        Debug.Log("hello"+www.downloadHandler.text+"ddd");
        }

    }

    public void CloseBack()
    {
        RankingCanvas.enabled = false;
    }

}
