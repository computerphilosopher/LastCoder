﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class creating : MonoBehaviour {
    public InputField New_IDInputField;
    public InputField New_PassInputField;
    public InputField New_PassInputField2;

    public Canvas ErrCanvas;

    public string CreUrl; 

    public Button ReButton;
    public Button SubmitButton;
	// Use this for initialization
	void Start () {
        CreUrl = "http://35.231.97.240:8080/etc.jsp";
       // CreUrl= "http://localhost:8000/register/etc.jsp";
        New_IDInputField = GameObject.Find("New_IDInputField").GetComponent<InputField>();
        New_PassInputField = GameObject.Find("New_PassInputField").GetComponent<InputField>();
        New_PassInputField2 = GameObject.Find("New_PassInputField2").GetComponent<InputField>();
        ErrCanvas.enabled = false;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SubClick() // 제출하기 누르면 
    {
        StartCoroutine(Createco());
    }

    public void RePress() // 돌아가기 누르면
    {
        ErrCanvas.enabled = false;
        SubmitButton.enabled = true;
    }

    IEnumerator Createco()
    {
        //WWWForm form;
        // UnityWebRequest www;

        //Debug.Log(New_PassInputField.text);
        //Debug.Log(New_PassInputField2.text);
        if (!New_IDInputField.text.Equals("") && !New_PassInputField.text.Equals("") && New_PassInputField.text.Equals(New_PassInputField2.text) ) { // 비번 같다면 전송 
            ErrCanvas.enabled = false;
            WWWForm form = new WWWForm();
            form.AddField("id", New_IDInputField.text);
            form.AddField("pw", New_PassInputField.text);

            UnityWebRequest www = UnityWebRequest.Post(CreUrl, form);

            yield return www.SendWebRequest();

            SceneManager.LoadScene(0);

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                Debug.Log("Form upload complete!");
            }

        }
        else // 틀리면 에러 메시지 출력
        {
            // Canvas 하나 더 만들어 줘야 됨
            // 비밀번호가 서로 다르면 
            ErrCanvas.enabled = true;    
        }

       
    }


}
