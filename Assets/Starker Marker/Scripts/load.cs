﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;
using Mono.Data.SqliteClient;
using System.Data;

public class load : MonoBehaviour {
    public string LoadUrl;
    public static int tempscore = 0;
    public string total;
    public string temp;
    public int hp, score;
    public float gameposx, gameposy, gameposz;
    public int scene;
    public int bandage;
    public int bullet;
    //starting str1;
    // Use this for initialization
    //starting st = GameObject.Find("").GetComponent<starting>();
    
	void Start () {
        LoadUrl = "http://35.231.97.240:8080/load.jsp";
    //     LoadUrl = "http://localhost:8000/register/load.jsp";
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void loadingClick()
    {
       //StartCoroutine(loading());
        LoadCall();
    }

    public void initialClick()
    { // 처음부터 하기면 다 초기화시켜주기
        InitialCall();
    }
    public void LoadCall()
    {
        string finder1 = "somegirl1";
        string finder2 = "somegirl2";
        total = starting.a;
      //  Debug.Log(total.IndexOf(finder1));
      //  Debug.Log(total.IndexOf(finder2));
      //  Debug.Log(total);
      //  Debug.Log(total.Substring(276, 30));
        temp = total.Substring(270, 40);
        Debug.Log(temp);
        char c = ' ';
        string[] status = temp.Split(c);
        hp = Convert.ToInt32(status[1]);
        tempscore = Convert.ToInt32(status[2]);
        gameposx = Convert.ToSingle(status[3]);
        gameposy = Convert.ToSingle(status[4]);
        gameposz = Convert.ToSingle(status[5]);
        scene = Convert.ToInt32(status[6]);
        bandage = Convert.ToInt32(status[7]);
        bullet = Convert.ToInt32(status[8]);
        if (scene.Equals(2))
        {
            score = tempscore;
            SceneManager.LoadScene(4);
        }
        else if (scene.Equals(3)){
            SceneManager.LoadScene(5);
        }
       /* for (int i = 1; i <= 8; i++)
        {
            
            Debug.Log("하나\n");
            Debug.Log(status[i] +"번호: "+ i);
        }
        */

    }
    public void InitialCall()
    {
        hp = 100;
        score = 0;
        gameposx = 0.0f;
        gameposy = 0.0f;
        gameposz = 0.0f;
        scene = 1;
        bandage = 0;
        bullet = 0;
        SceneManager.LoadScene(3);
    }

   IEnumerator loading()
   {
       
        WWWForm form = new WWWForm();
        UnityWebRequest www = UnityWebRequest.Post(LoadUrl, form);
        yield return www.SendWebRequest();
        Debug.Log("시험해보자"+temp);
        temp = www.downloadHandler.text;

   }

   public void exit()
    {
        SceneManager.LoadScene(0);
    }

    private enum StageIndex
    {
        Stage1_Start = 1,
        Stage2_Start = 18,
        Stage3_Start = 37,
    }

    private void LocalDBInitialize()
    {
        string dirPath = Application.dataPath + "/../Problem";
        string dbPath = "URI=file:" + dirPath + "/Problem.db";

        IDbConnection dbConnection = (IDbConnection)new SqliteConnection(dbPath);

        dbConnection.Open();

        IDbCommand dbCommand = dbConnection.CreateCommand();

        for (int i = (int)StageIndex.Stage1_Start; i < (int)StageIndex.Stage2_Start; i++)
        {
            string sqlQuery = "UPDATE " + "Problem " + "SET " + "isSolved=1 " + "WHERE " + "problemNumber=" + i + ";";
            dbCommand.CommandText = sqlQuery;

            dbCommand.ExecuteNonQuery();

            dbCommand.Dispose();
            dbConnection.Close();
        }

    }
}
