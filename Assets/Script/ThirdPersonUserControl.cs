using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
//using UnityStandardAssets.CrossPlatformInput;

//namespace UnityStandardAssets.Characters.ThirdPerson
//C# 언어는 namespaces 기능으로 이 문제를 확고하게 해결합니다. 
//네임스페이스는 클래스 이름에 선택한 접두어를 사용하여 참조하는 클래스의 모음입니다.
//{

//RequireComponent 속성은 요구되는 의존 컴포넌트를 자동으로 추가해줍니다.
//[RequireComponent(typeof (ThirdPersonCharacter))] //


    public class ThirdPersonUserControl : MonoBehaviour
{
    ThirdPersonCharacter m_Character;
    //private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
    private Transform m_Cam;                  // A reference to the main camera in the scenes transform
    private Vector3 m_CamForward;             // The current forward direction of the camera
    private Vector3 m_Move;
    private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
    public static bool isAiming;
    public static bool isCrouching;
    public GameObject Marker;


    private void Start()
    {
        // get the transform of the main camera
        if (Camera.main != null)
        {
            m_Cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        }

        // get the third person character ( this should never be null due to require component )
        m_Character = GetComponent<ThirdPersonCharacter>();
    }


    //스크립트가 enabled 상태일때, 매 프레임마다 호출됩니다. 일반적으로 가장 빈번하게 사용되는 함수이며, 
    //물리 효과가 적용되지 않은 오브젝트의 움직임이나 단순한 타이머, 키 입력을 받을 때 사용됩니다.
    private void Update()
    {
        if (!m_Jump)
        {
            //CrossPlatformInputManager : mobile 설정을 해주면 모바일로도 input 가능
            m_Jump = Input.GetButtonDown("Jump");
        }

    }


    // Fixed update is called in sync with physics
    public void FixedUpdate()
    {
        // read inputs
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        bool crouch = Input.GetKey(KeyCode.C);
        bool aim = Input.GetButton("Aim");
        isAiming = aim;
        isCrouching = crouch;

        // calculate move direction to pass to character = character 이동 방향 계산
        if (m_Cam != null)
        {
            // calculate camera relative direction to move: 이동할 카메라 방향 계산
            //scale - 두 벡터의 요소를 곱함
            //.normalized 벡터를 정규화, 방향을 같지만 크기가 1인 벡터 반환
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized; // 0, 0, 1
            m_Move = v * m_CamForward + h * m_Cam.right; //(cam.right = 1, 1, 0
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            m_Move = v * Vector3.forward + h * Vector3.right;
        }
#if !MOBILE_INPUT
        // walk speed multiplier
        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;

        //player reset
        if (Input.GetKeyDown("p") && Input.GetKey(KeyCode.LeftShift))
        {
            m_Character.transform.Translate(h*5, 0, v*5, Space.World);   
            
        }
#endif

        // pass all parameters to the character control script

        //if reload?

        //animation 실행 중이 아니거나 장전 중 이동 가능
        if (!m_Character.IsAnimationing()) 
        {
            m_Character.Turning(aim);
            m_Character.Move(m_Move, crouch, m_Jump, aim);
        }

        m_Jump = false;

        
        Marker.transform.position = new Vector3(this.transform.position.x, Marker.transform.position.y, this.transform.position.z);
        Marker.transform.forward = this.transform.forward;
        
    }

    public bool setAim(bool aim)
    {
        if (aim)
        {
            isAiming = true;
        }
        else
        {
            isAiming = false;
        }
        return isAiming;
    }
 
} 
//}
