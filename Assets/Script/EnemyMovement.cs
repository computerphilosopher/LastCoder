﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{

    Transform player; 
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    NavMeshAgent nav;  //player 추적

    float distance;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform; //player 태그 찾아 위치 알려줌
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();

        nav = GetComponent<NavMeshAgent>();
        nav.SetDestination(player.position);
    }

    void Update() //물리 효과를 시간에 따라 맞추지 않으므로 default update
    {
        distance = Vector3.Distance(player.position, transform.position);

        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            if (nav.isOnNavMesh)
            {
                //GameObject.FindGameObjectWithTag("StylishZombie")
                nav.SetDestination(player.position); //player 쪽으로 가기 위한 설정
            }
            else
            {
                nav.enabled = true;
            }
        }
        else
        {
            //nav.enabled = false;
        }
    }
}
