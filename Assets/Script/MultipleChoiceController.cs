﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultipleChoiceController : MonoBehaviour
{

    public int problemNumber;
    private Dropdown dropDown;
    private Button submitButton;

    private Text questionText;
    private DBParser parser;

    const int optionCount = 4;
    const int tryLimit = 2; //도전 제한 횟수

    int tryCount;
    public int itemID;

    void Start()
    {
        GetComponent<Canvas>().enabled = false;
        GetComponent<MultipleChoiceController>().enabled = false;

        tryCount = 0;
    }

    public void OnEnable()
    {
        GetComponent<Canvas>().enabled = true;
        dropDown = GetComponentInChildren<Dropdown>();
        questionText = GameObject.Find("QuestionText").GetComponentInChildren<Text>();

        string dbPath = Application.dataPath + "/../Problem/";
        parser = new DBParser(problemNumber, dbPath);


        submitButton = gameObject.GetComponentInChildren<Button>();
        submitButton.onClick.AddListener(ButtonListener);

        Debug.Log(GetComponent<Canvas>().enabled);

        questionText.text = parser.Question;
        fillDropDown();
    }

    private void ButtonListener()
    {
        Marker marker = new Marker(parser, dropDown.value + 1, parser.RightChoice, itemID);
        marker.GiveReward(problemNumber);

        if (marker.IsCorrect())
        {
            submitButton.GetComponentInChildren<Text>().text = "correct!";
            submitButton.enabled = false;
        }
        else
        {
            tryCount++;
        }

        if(tryCount > tryLimit)
        {
            submitButton.GetComponentInChildren<Text>().text = "횟수 초과!";
            submitButton.enabled = false;

            parser.ReadDB();

            parser.UpdateSolvedProblem(problemNumber);

        }

    }

    private void fillDropDown()
    {
        for (int i = 0; i < optionCount; i++)
        {
            dropDown.options[i].text = parser.GetDropDownText(i);
        }

        dropDown.captionText.text = dropDown.options[0].text;
    }

    void Update()
    {

    }

    public int ProblemNumber
    {
        get { return problemNumber; }
        set { problemNumber = value; }
    }
}

