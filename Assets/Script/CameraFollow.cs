﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    //public Transform target; //camera가 추적할 target 
    private Transform target;
    public float smoothing = 0f; //camera가 얼마나 빈틉없이 추적할 것인지
    Vector3 offset;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        gameObject.transform.position = target.position; //camera의 position을 target(player)의 position 만큼 이동
        gameObject.transform.position += new Vector3(1, 15, -22);
        offset = transform.position - target.position;
    }
        
    void FixedUpdate() //default update를 쓰면 player와 실행되는 시간대가 다르다.
    {
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing*Time.deltaTime);
    }

}
 