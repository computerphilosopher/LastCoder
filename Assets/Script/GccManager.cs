﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using Mono.Data.SqliteClient;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;


class GccManager : MonoBehaviour
{
    private Process compileProcess;
    private Process excuteProcess;
    private string relativePath;

    public int problemNumber;
    public int itemID;

    private Button submitButton;

    private Text errorMsg;
    private Text stdOut;
    private Text tutorial;
    private Text question;

    private string outputText;
    private InputField codingField;

    DBParser parser;

    void Awake()
    {
        GetComponent<Canvas>().enabled = false;
        relativePath = Application.dataPath + "/../bin";
        errorMsg = GameObject.Find("ErrorMSG").GetComponentInChildren<Text>();
        errorMsg.text = "";

        stdOut = GameObject.Find("Output").GetComponentInChildren<Text>();
        stdOut.text = "";

        submitButton = GetSubmitButton();
        submitButton.onClick.AddListener(ButtonListener);
        GetComponent<GccManager>().enabled = false;
    }

    void OnEnable()
    {
        string dbPath = relativePath + "/../Problem/";
        parser = new DBParser(problemNumber, dbPath);
        
        question = GameObject.Find("CodeQuestion").GetComponentInChildren<Text>();
        question.text = parser.Question;
        UnityEngine.Debug.Log(parser.Question);

        tutorial = GameObject.Find("Tutorial").GetComponentInChildren<Text>();
        //tutorial.text = ReadDefaultText(parser.TutorialPath);
        tutorial.text = parser.Tutorial;

        codingField = GameObject.Find("InputField").GetComponent<InputField>();
        codingField.text = ReadDefaultText(parser.SourcePath);

    }

    private void ClearAllText()
    {
        errorMsg.text = "";
        stdOut.text = "";

    }

    private void ButtonListener()
    {

        ClearAllText();
        Compile();
        Excute();
        Marking();

        parser.ReadDB();
        if (parser.IsSolved)
        {
            submitButton.GetComponentInChildren<Text>().text = "correct!";
            submitButton.enabled = false;
        }
    }

    private Button GetSubmitButton()
    {
        Button b = gameObject.GetComponentInChildren<Button>();

        return b;
    }

    private string ReadDefaultText(string path)
    {
        string fileText = "";

        try
        {
            StreamReader reader = new StreamReader(path);
            fileText = reader.ReadToEnd();
            reader.Close();

        }
        catch (FileNotFoundException e)
        {
            fileText = "file not found";
            UnityEngine.Debug.Log(e.Message);
        }
        //UnityEngine.Debug.Log(fileText);

        return fileText;

    }

    private void WriteCFile()
    {
        StreamWriter writer = new StreamWriter(parser.SourcePath, false);

        string inputString = GetComponentInChildren<InputField>().text;

        writer.WriteLine(inputString);

        writer.Close();
    }

    public void Compile()
    {

        WriteCFile();

        compileProcess = new Process();
        compileProcess.StartInfo = new ProcessStartInfo(relativePath + "/gcc.bat", problemNumber.ToString());
        compileProcess.StartInfo.UseShellExecute = false;
        compileProcess.StartInfo.CreateNoWindow = true;
        compileProcess.StartInfo.RedirectStandardError = true;

        compileProcess.Start();

        string compileError = compileProcess.StandardError.ReadToEnd();
        compileProcess.StandardError.Close();

        //compileError.Split("error");

        string[] error = compileError.Split(new string[] { "error" }, StringSplitOptions.None);
        compileProcess.StandardError.Close();

        if (!compileError.Equals(""))
        {
            errorMsg.text = error[1];
        }

        errorMsg.text = compileError;
        compileProcess.WaitForExit();

    }

    private void Excute()
    {
        excuteProcess = new Process();

        excuteProcess.StartInfo = new ProcessStartInfo(relativePath + "/redir_input.bat", relativePath + " " + parser.InputPath);


        excuteProcess.StartInfo.CreateNoWindow = true;
        excuteProcess.StartInfo.UseShellExecute = false;

        excuteProcess.Start();

        excuteProcess.WaitForExit(1200);

        excuteProcess.Dispose();

        try
        {
            outputText = File.ReadAllText(relativePath + "/output.txt");
            stdOut.text = outputText;
            //File.Delete(relativePath + "/output.txt");

        }

        catch (FileNotFoundException e)
        {
            stdOut.text = "";
            UnityEngine.Debug.Log(e.Message);
        }

        catch (IOException e)
        {

            string terminateCommand = "/C taskkill /f /im test.exe";

            Process cmd = new Process();
            cmd.StartInfo = new ProcessStartInfo("cmd.exe", terminateCommand);
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;

            cmd.Start();


            UnityEngine.Debug.Log(e.Message);
        }


    }

    private void Marking()
    {
        Text resultMessage = GameObject.Find("Result").GetComponentInChildren<Text>();

        Marker marker = new Marker(outputText, parser, itemID);
        resultMessage.text = marker.ShowResult();

        marker.GiveReward(problemNumber);

    }
 
    void Destroy()
    {
        submitButton.onClick.RemoveAllListeners();

    }
    /* 채점 클래스 */

    public int ProblemNumber
    {
        get { return problemNumber; }
        set { problemNumber = value; }
    }

}


public class Marker
{
    private string userSubmission;
    private string answerPath;
    private string answer;

    private DBParser parser;

    private string result;

    private int itemID;

    private enum ItemNum {
        BULLET = 22
    }

    const int CHOICE_POINT = 50;
    const int CODING_POINT = 100;
    int userChoice;
    int rightChoice;
    
    public Marker(string userSubmission, DBParser parser, int itemID)
    {

        this.parser = parser;
        this.userSubmission = userSubmission;
        this.answerPath = parser.AnswerPath;
        this.itemID = itemID;
        answer = File.ReadAllText(this.answerPath);

        parser.ReadDB();

    }
   

    public Marker(DBParser parser, int userChoice, int rightChoice, int itemID)
    {
        this.userChoice = userChoice;
        this.rightChoice = rightChoice;
        this.parser = parser;

        this.itemID = itemID;

    }


    public bool IsCorrect()
    {
        string problemType = parser.ProblemType;
        if (problemType.Equals("coding"))
        {
            if (userSubmission.Equals(answer))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        else //객관식 문제일 때
        {
            if (userChoice == rightChoice)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }

    public void GiveReward(int problemNumber)
    {
        if (IsCorrect())
        {
            PlayerItem playerItem = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerItem>();

            if(parser.ProblemType == "coding")
            {
                ScoreManager.score += CODING_POINT;
            }

            else //객관식이면
            {
                ScoreManager.score += CHOICE_POINT;

            }

            if (itemID == (int)ItemNum.BULLET)
            {

                int prizeBullet = 15;

                //15발중 장전하고 남은 총알 
                int extraBullet = ((BulletManager.bullet + prizeBullet) % BulletManager.magazineBulletSize) * ((BulletManager.bullet + prizeBullet) / BulletManager.magazineBulletSize);

                if (BulletManager.bullet + prizeBullet <= BulletManager.magazineBulletSize)
                {
                    BulletManager.bullet = BulletManager.bullet + prizeBullet;
                }
                else
                {
                    BulletManager.bullet = BulletManager.magazineBulletSize;

                    if (BulletManager.currentBullet+extraBullet <= BulletManager.maxBullet)
                    {
                        BulletManager.currentBullet += extraBullet;
                    }
                    else
                    {
                        BulletManager.currentBullet = BulletManager.maxBullet;
                    }
                }
                           
            }

            else
            {
                playerItem.AddItem(itemID);
            }
 
            parser.UpdateSolvedProblem(problemNumber);
        }
        else
        {
            return;
        }
    }



    public string ShowResult()
    {

        if (String.IsNullOrEmpty(userSubmission))
        {
            result = "시간 초과";
        }

        else if (userSubmission.Equals(answer))
        {
            result = "정답입니다.";
        }

        else
        {
            result = ("틀렸습니다.");
        }

        return result;

    }

}


public class DBParser
{
    string dirPath;
    string dbPath;

    private string sourcePath;
    private string inputPath;

    private string answerPath;
    private string questionPath; // 파일에서 읽지 않고 DB에서 직접 읽어올 예정
    private string tutorialPath;

    private int problemNumber;
    private string problemType;

    private string question;
    private string tutorial;

    private string[] choice;

    const int CHOICE_COUNT = 4;
    const int FALSE = 0;
    const int TRUE = 1;

    private bool isSolved;

    private int rightChoice;

    private string primaryKey;

    private enum DBIndex
    {

        PROBLEM_NUMBER,
        PROBLEM_NAME,

        SOURCE_PATH,
        INPUT_PATH,
        ANSWER_PATH,

        IS_SOLVED,
        PROBLEM_TYPE,
        CHOICE1,
        CHOICE2,
        CHOICE3,
        CHOICE4,
        QUESTION,
        RIGHT_CHOICE,
        TUTORIAL
    }

    public DBParser(int problemNumber, string dirPath)
    {
        this.dirPath = dirPath;
        dbPath = "URI=file:" + dirPath + "Problem.db";
        this.problemNumber = problemNumber;

        ReadDB();

    }

    public DBParser(int problemNumber)
    {
        this.dirPath = Application.dataPath + "/../Problem";
        dbPath = "URI=file:" + dirPath + "/Problem.db";
        
        this.problemNumber = problemNumber;

        ReadDB();
    }
    public void ReadDB()
    {

        IDbConnection dbConnection = (IDbConnection)new SqliteConnection(dbPath);

        dbConnection.Open();

        IDbCommand dbCommand = dbConnection.CreateCommand();

        //string sqlQuery = "select " + "sourcePath, inputPath, answerPath, isSolved" + "from Problem " + "where " + "problemNumber=" + problemNumber + ";";
        string sqlQuery = "SELECT " + "* " + "FROM Problem " + "WHERE " + "problemNumber = '" + problemNumber + "';";

        dbCommand.CommandText = sqlQuery;

        IDataReader dataReader = dbCommand.ExecuteReader();

        dataReader.Read();

        problemType = dataReader.GetString((int)DBIndex.PROBLEM_TYPE);

        if (problemType.Equals("coding"))
        {
            sourcePath = dirPath + problemNumber.ToString() + "/" + dataReader.GetString((int)DBIndex.SOURCE_PATH);
            inputPath = dirPath + problemNumber.ToString() + "/" + dataReader.GetString((int)DBIndex.INPUT_PATH);
            answerPath = dirPath + problemNumber.ToString() + "/" + dataReader.GetString((int)DBIndex.ANSWER_PATH);
        tutorial = dataReader.GetString((int)DBIndex.TUTORIAL);
        }

        question = dataReader.GetString((int)DBIndex.QUESTION);
 
        //tutorial = dataReader.GetString((int)DBIndex.TUTORIAL);
        rightChoice = dataReader.GetInt32((int)DBIndex.RIGHT_CHOICE);

        choice = new string[CHOICE_COUNT];

        if (problemType.Equals("choice"))
            for (int i = 0; i < 4; i++)
            {
                int dbIndex = (int)DBIndex.CHOICE1;
                try
                {
                    choice[i] = dataReader.GetString(dbIndex + i);
                }

                catch (NullReferenceException e)
                {
                    choice[i] = "";
                    UnityEngine.Debug.Log(e.Message);
                }
            }

        questionPath = dirPath + problemNumber.ToString() + "/" + "question.txt";
        tutorialPath = dirPath + problemNumber.ToString() + "/" + "tutorial.txt";

        if (dataReader.GetInt16((int)DBIndex.IS_SOLVED) == FALSE)
        {
            isSolved = false;
        }

        else
        {
            isSolved = true;
        }

        dataReader.Close();
        dataReader = null;

        dbCommand.Dispose();
        dbCommand = null;

        dbConnection.Close();
        dbConnection = null;


    }

    public void UpdateSolvedProblem(int problemNumber)
    {

        ReadDB();

        IDbConnection dbConnection = (IDbConnection)new SqliteConnection(dbPath);

        dbConnection.Open();

        IDbCommand dbCommand = dbConnection.CreateCommand();

        string sqlQuery = "UPDATE " + "Problem " + "SET " + "isSolved=1 " + "WHERE " + "problemNumber=" + problemNumber + ";";

        UnityEngine.Debug.Log(sqlQuery);

        dbCommand.CommandText = sqlQuery;

        dbCommand.ExecuteNonQuery();

        dbCommand.Dispose();
        dbConnection.Close();

    }

    public string GetDropDownText(int index)
    {
        try
        {
            return choice[index];
        }
        catch (ArgumentOutOfRangeException e)
        {
            UnityEngine.Debug.Log(e.Message);
            return "index error";
        }
    }

    public int ProblemNumber
    {
        get { return problemNumber; }
    }

    public string InputPath
    {
        get { return inputPath; }
    }

    public string AnswerPath
    {
        get { return answerPath; }
    }

    public string SourcePath
    {
        get { return sourcePath; }
    }

    public string QuestionPath
    {
        get { return questionPath; }
    }

    public string TutorialPath
    {
        get { return tutorialPath; }
    }

    public string Question
    {
        get { return question; }
    }

    public string Tutorial
    {
        get { return tutorial; }
    }

    public bool IsSolved
    {
        get { return isSolved; }
    }

    public string Choice1
    {
        get { return choice[0]; }
    }

    public string Choice2
    {
        get { return choice[1]; }
    }

    public string Choice3
    {
        get { return choice[2]; }
    }

    public string Choice4
    {
        get { return choice[3]; }
    }

    public int RightChoice
    {
        get { return rightChoice; }
    }

    public string ProblemType
    {
        get { return problemType; }
    }
}



