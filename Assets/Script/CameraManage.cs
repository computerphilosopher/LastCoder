﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CameraManage : MonoBehaviour {

    public Camera[] cams;

    public void MainCamera()
    {
        cams[0].enabled = true;
        cams[1].enabled = false;
        cams[2].enabled = false;
        cams[3].enabled = false;
    }

    public void FirstC() 
    {
        cams[0].enabled = false;
        cams[1].enabled = true;
        cams[2].enabled = false;
        cams[3].enabled = false;
    }

    public void SecondC()
    {
        cams[0].enabled = false;
        cams[1].enabled = false;
        cams[2].enabled = true;
        cams[3].enabled = false;
    }
     
    public void ThirdC()
    {
        cams[0].enabled = false;
        cams[1].enabled = false;
        cams[2].enabled = false;
        cams[3].enabled = true;
    }

    private void Update()
    {
        if (Input.GetKey("0"))
        {
            FirstC(); 
        }
        else if (Input.GetKey("-"))
        {
            SecondC();
        }

        else if (Input.GetKey("="))
        {
            ThirdC();
        }
        else
        {
            MainCamera();
        }
    }

}
