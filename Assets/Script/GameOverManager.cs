﻿
using UnityEngine;

public class GameOverManager : MonoBehaviour { // player 죽고 난 뒤 Ending Credit

    //public PlayerHealth playerHealth;
    private PlayerHealth playerHealth;
    
    public float restartDelay = 5f; //player 죽은 후 delay 후 restart

    Animator anim;
    float restartTimer;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
    }

    void Update () {
		if(playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
            restartTimer += Time.deltaTime; // restart timer, counting
            if(restartTimer >= restartDelay) 
            {
#pragma warning disable CS0618 // 형식 또는 멤버는 사용되지 않습니다. -> warning을 없애기 위해 추가함
                //Application.LoadLevel(Application.loadedLevel); // load된 level에서 scene이 reload됨
#pragma warning restore CS0618 // 형식 또는 멤버는 사용되지 않습니다.
            }
        }
	}
}