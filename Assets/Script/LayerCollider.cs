﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerCollider : MonoBehaviour {
    public Camera[] cams;

    public void MainCamera()
    {
        cams[0].enabled = true;
        cams[1].enabled = false;
        cams[2].enabled = false;
        cams[3].enabled = false;
    }

    public void FirstC()
    {
        cams[0].enabled = false;
        cams[1].enabled = true;
        cams[2].enabled = false;
        cams[3].enabled = false;
    }

    public void SecondC()
    {
        cams[0].enabled = false;
        cams[1].enabled = false;
        cams[2].enabled = true;
        cams[3].enabled = false;
    }

    public void ThirdC()
    {
        cams[0].enabled = false;
        cams[1].enabled = false;
        cams[2].enabled = false;
        cams[3].enabled = true;
    }


    private void OnCollisionEnter(Collision collision)
    {
     
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "1stBottom")
        {
            FirstC();
        }
        else if (collision.gameObject.tag == "SecondBottom")
        {

            SecondC();
        }
        else if (collision.gameObject.tag == "ThirdBottom")
        {
            ThirdC();
        }
    
    }
    private void OnCollisionExit(Collision collision)
    {
        MainCamera();
    }
}
