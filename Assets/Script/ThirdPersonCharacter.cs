using System;
using UnityEngine;
using System.Collections;



//namespace UnityStandardAssets.Characters.ThirdPerson
//{
//[RequireComponent(typeof(Rigidbody))]
//[RequireComponent(typeof(CapsuleCollider))]
//[RequireComponent(typeof(Animator))]
public class ThirdPersonCharacter : MonoBehaviour
{
    //강제로 Unity가 비공개 필드를 직렬화합니다.
    [SerializeField] float m_MovingTurnSpeed = 360; //움직이는 turn speed
    [SerializeField] float m_StationaryTurnSpeed = 180; //고정된 turn speed
    [SerializeField] float m_JumpPower = 12f;
    [Range(1f, 4f)] [SerializeField] float m_GravityMultiplier = 2f;
    [SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
    [SerializeField] float m_MoveSpeedMultiplier = 1f;  //이동 속도 곱하기
    [SerializeField] float m_AnimSpeedMultiplier = 1f;
    [SerializeField] float m_GroundCheckDistance = 0.1f;
    [SerializeField] float timeBetweenAnim = 2; // Anim delay

    Rigidbody m_Rigidbody;
    Animator m_Animator;
    float m_OrigGroundCheckDistance;
    const float k_Half = 0.5f;
    float m_TurnAmount;
    float m_ForwardAmount;
    Vector3 m_GroundNormal;
    float m_CapsuleHeight;
    Vector3 m_CapsuleCenter;
    CapsuleCollider m_Capsule;
    bool m_Crouching;
    bool m_Aim;

    public static bool isAnim;  //animation 실행 중인지
    public static bool m_IsGrounded;

    int floorMask; //지형의 layer를 이걸로 설정해줘야 camera의 ray를 찾을 수 있음
    float camRayLength = 100f;

    float inputH; //상하좌우 키보드 입력을 받기위해
    float inputV;
    float zv;
    float zh;
    float xv;
    float xh;
    Vector3 m_V;

    //add
    public static Vector3 shootForward;


    public GameObject equippingGun; //장착한 Item에 대한 object


    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Capsule = GetComponent<CapsuleCollider>();
        m_CapsuleHeight = m_Capsule.height;
        m_CapsuleCenter = m_Capsule.center;
        floorMask = LayerMask.GetMask("Floor"); //gameobject의 layer를 floor로 설정
        isAnim = false;

        
        //m_Rigidbody.constraints : 이 리짓 바디의 시뮬레이션에 허용되는 자유도를 제어합니다.
        //x,y,z 회전 제어
        m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        m_OrigGroundCheckDistance = m_GroundCheckDistance;
        
    }

    public void FixedUpdate()
    {
        IsAnimationing();
    }

    public bool IsAnimationing()
    {
        //Animation 실행 중인지 확인 
        if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Combat Roll[Rifle]") ||
           m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Push_Kick") ||
           m_Animator.GetCurrentAnimatorStateInfo(0).IsName("ItemPickup_Ground") ||
           m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Fire[Rifle]") 
           //m_Animator.GetCurrentAnimatorStateInfo(0).IsName("RifleReload[Rifle]")
           )    
        {
            isAnim = true;
        }
        else
        {
            isAnim = false;
        }
        return isAnim;
    }
    public void Move(Vector3 move, bool crouch, bool jump, bool aim)
    {

        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");

        // convert the world relative moveInput vector into a local-relative
        // turn amount and forward amount required to head in the desired
        // direction.
        //move.magnitude : 이 벡터의 길이를 반환합니다 (읽기 전용). 벡터의 길이는의 제곱근입니다(x * x + y * y + z * z).
        if (move.magnitude > 1f) move.Normalize();
        move = transform.InverseTransformDirection(move); //a direction를 월드 공간에서 로컬 공간으로 변환합니다 . Transform.TransformDirection의 반대입니다.

        CheckGroundStatus();
        move = Vector3.ProjectOnPlane(move, m_GroundNormal);


        m_TurnAmount = Mathf.Atan2(move.x, move.z);
        m_ForwardAmount = move.z;


        ApplyExtraTurnRotation();

        // control and velocity handling is different when grounded and airborne:
        if (m_IsGrounded)
        {
            HandleGroundedMovement(crouch, jump);
        }
        else
        {
            HandleAirborneMovement();
        }

        
        SetAim(aim);
        ScaleCapsuleForCrouching(crouch);
        PreventStandingInLowHeadroom();

        // send input and other state parameters to the animator
        UpdateAnimator(move); 
    }
    

    void SetAim(bool aim)
    {
        if (aim)
        {
            m_Aim = true;
        }
        else
        {
            m_Aim = false;
        }
    }


    // mouse pointer에 따라 player turning 설정, player가 바라보는 곳으로 player를 turn 시킨다.
    public void Turning(bool aim)
    {
        if (aim && m_IsGrounded && !m_Crouching && !isAnim && equippingGun.activeSelf)
        {
            Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition); //Camera 에서 나오는 Ray, 마우스의 위치의 포인터
            RaycastHit floorHit; //위의 point 정보를 찾기위해
            if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask)) //
            {
                Vector3 playerToMouse = floorHit.point - transform.position; //player ~ mousePointer 까지의 vector3
                playerToMouse.y = 0f; //player가 뒤로 기댄 상태가 되지 않게
                Quaternion newRoation = Quaternion.LookRotation(playerToMouse); //player 회전 지정
                m_Rigidbody.MoveRotation(newRoation);
                m_V = Vector3.Scale(playerToMouse, new Vector3(1, 0, 1)).normalized;

                //add
                shootForward = floorHit.point - GameObject.FindGameObjectWithTag("Muzzle").transform.position;

            }
        }
    }

    void ScaleCapsuleForCrouching(bool crouch)
    {
        if (m_IsGrounded && crouch)
        {
            if (m_Crouching) return;
            m_Capsule.height = m_Capsule.height / 2f;
            m_Capsule.center = m_Capsule.center / 2f;
            m_Crouching = true;
        }

        else
        {
            //Ray는 origin 에서 시작해서 direction 방향으로 나아가는 무한대 길이의 선입니다
            Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
            float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
            if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                //capsulecollider 보다 작은 곳에 들어가면 crouching 되게 하는 것 같다
                m_Crouching = true;
                return;
            }
            m_Capsule.height = m_CapsuleHeight;
            m_Capsule.center = m_CapsuleCenter;
            m_Crouching = false;
        }
    }

    void PreventStandingInLowHeadroom()
    {
        // 웅크리는 구역에서 일어나지 못하게
        if (!m_Crouching)
        {
            Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
            float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;
            if (Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                m_Crouching = true;
            }
        }
    }


    void UpdateAnimator(Vector3 move)
    {
        // update the animator parameters
        
        m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
        m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
        m_Animator.SetBool("Crouch", m_Crouching);
        m_Animator.SetBool("OnGround", m_IsGrounded);
        m_Animator.SetBool("EquippingGun", equippingGun.activeSelf); // object가 활성화 상태인지 아닌지로 T or F

        //crouch or jump 시는 Aim을 false로
        if (m_Crouching || !m_IsGrounded)
        {
            m_Animator.SetBool("Aim", false);
        }
        m_Animator.SetBool("Aim", m_Aim);


        m_Animator.SetFloat("inputH", inputH);
        m_Animator.SetFloat("inputV", inputV);


        zv = m_V.z * inputV;
        zh = m_V.z * inputH;
        xv = m_V.x * inputV;
        xh = m_V.x * inputH;


        m_Animator.SetFloat("zv", zv);
        m_Animator.SetFloat("zh", zh);
        m_Animator.SetFloat("xv", xv);
        m_Animator.SetFloat("xh", xh);

        AnimationMotion();
        

        if (!m_IsGrounded)
        {
            m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
        }

        // calculate which leg is behind, so as to leave that leg trailing in the jump animation
        // (This code is reliant on the specific run cycle offset in our animations,
        // and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
        float runCycle =
            Mathf.Repeat(
                m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
        float jumpLeg = (runCycle < k_Half ? 1 : -1) * m_ForwardAmount;
        if (m_IsGrounded)
        {
            m_Animator.SetFloat("JumpLeg", jumpLeg);
        }

        // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
        // which affects the movement speed because of the root motion.
        //걷기, 뛰기 속도 조정
        if (m_IsGrounded && move.magnitude > 0)
        {
            m_Animator.speed = m_AnimSpeedMultiplier;
        }
        else
        {
            // don't use that while airborne
            m_Animator.speed = 1;
        }
    }


    // anim 시 turning 안되게 해야됨
    private void AnimationMotion() // player motion and action
    {
       
        if (Input.GetKeyDown("g")) //gun 장착 및 해제
        { 
            equippingGun.SetActive(!equippingGun.activeSelf);
        }
        
        if (Input.GetKeyDown("e") && !isAnim) //ground Item pick up
        {
            m_Animator.Play("ItemPickup_Ground", -1, 0f);
        }

        if (Input.GetKeyDown("1") && !isAnim) //Push_Kick
        {
            m_Animator.Play("Push_Kick", -1, 0f);
        }

        if (Input.GetKeyDown("z") && !isAnim && m_IsGrounded) // 구르기
        {
            m_Animator.Play("Combat Roll[Rifle]", -1, 0f);
        }

        if (Input.GetMouseButtonDown(0)) //Shooting Rifle, mouse left button, // MouseButtonDown -> 한번클릭, mouseButton -> 클릭 중 동작
        {

            if (ThirdPersonUserControl.isAiming && !ThirdPersonUserControl.isCrouching && m_IsGrounded && BulletManager.ableShoot)
            {
                if (equippingGun.activeSelf)
                {
                    //사격
                    m_Animator.Play("Fire[Rifle]", -1, 0f);
                }
            }
        }
        
        if (BulletManager.reloadGun) //장전
        {
            m_Animator.Play("RifleReload[Rifle]", -1, 0f);
            BulletManager.reloadGun = false;
        }
    }
 
    /*
    IEnumerator TimeBetweenAnim()
    {
        delay = false;
        yield return new WaitForSeconds(2);
        delay = true;
    }
    */

    void HandleAirborneMovement() // 공중에 있을 때
    {
        // apply extra gravity from multiplier:
        Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
        m_Rigidbody.AddForce(extraGravityForce);

        m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
    }


    void HandleGroundedMovement(bool crouch, bool jump) // jump 시
    {
        // check whether conditions are right to allow a jump:
        if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded")) //GetCurrentAnimatorStateInfo : animator layer의 현재 정보를 가져옴
        {
            m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
            m_IsGrounded = false;
            m_Animator.applyRootMotion = false;
            m_GroundCheckDistance = 0.1f;
        }
    }


    void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);

        if (!m_Aim)
        {
            transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
        }
    }


    public void OnAnimatorMove()
    {
        // we implement this function to override the default root motion.
        // this allows us to modify the positional speed before it's applied.
        if (m_IsGrounded && Time.deltaTime > 0)
        {
            Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

            // we preserve the existing y part of the current velocity.
            v.y = m_Rigidbody.velocity.y;
            m_Rigidbody.velocity = v;
        }
    }


    void CheckGroundStatus()
    {
        RaycastHit hitInfo;
#if UNITY_EDITOR
        // helper to visualise the ground check ray in the scene view
        Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
        {
            m_GroundNormal = hitInfo.normal;
            m_IsGrounded = true;
            m_Animator.applyRootMotion = true;
        }
        else
        {
            m_IsGrounded = false;
            m_GroundNormal = Vector3.up;
            m_Animator.applyRootMotion = false;
        }
    }
}
