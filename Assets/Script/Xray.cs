﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Xray : MonoBehaviour
{

    public GameObject xRayCamera;



    // Use this for initialization
    void Start()
    {
        xRayCamera.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButton("1st"))
        {
            xRayCamera.SetActive(true);
        }
        else
        {
            xRayCamera.SetActive(false);
        }
    }

}
