﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class PlayerShooting : MonoBehaviour
{
    
    public int damagePerShot = 25; // 총 1발의 damage
    public float timeBetweenBullets = 0.15f; //총의 발사 속도
    public float range = 100f; //총알의 이동 거리
 
    Ray ShootRay; // 총 발사 시 광선
    RaycastHit shootHit; //
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;

    float timer;
    int shootableMask;  //쏠 수 있는 것들만 맞추도록
    int firstMask;  //건물 1층의 외각
    int secondMask;  //2층
    int thirdMask;  //3층
    float effectsDisplayTime = 0.2f;


    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable"); // 쏠 수 있는 객체로 설정
        firstMask = LayerMask.GetMask("1st");
        secondMask = LayerMask.GetMask("2nd");
        thirdMask = LayerMask.GetMask("3st");
        gunParticles = GetComponent<ParticleSystem>();
        gunLine = GetComponent<LineRenderer>();
        gunAudio = GetComponent<AudioSource>();
        gunLight = GetComponent<Light>();
    }

    void Update()
    {

        timer += Time.deltaTime;

        //left mouse button + over timer
        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets)
        {
            if (ThirdPersonUserControl.isAiming && !ThirdPersonUserControl.isCrouching && ThirdPersonCharacter.m_IsGrounded && !ThirdPersonCharacter.isAnim)
            {
                if (BulletManager.ableShoot)
                {
                    BulletManager.bullet = BulletManager.bullet - 1;
                    Shoot();
                }
            }
        }

        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects();
        }
    }

    public void DisableEffects()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }

    void Shoot()
    {

        timer = 0f;

        gunAudio.Play();
        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position); //총선, 시작하는 지점

        //ShootRay.origin = transform.position; //광선 시작
        ShootRay.origin = GameObject.FindGameObjectWithTag("Muzzle").transform.position;

        Vector3 v3 = ThirdPersonCharacter.shootForward;
        //ShootRay.direction = v3.normalized;  //최신
        ShootRay.direction = transform.forward; //광선의 방향, 기존방식
       

        if ((Physics.Raycast(ShootRay, out shootHit, range, shootableMask)) || (Physics.Raycast(ShootRay, out shootHit, range, firstMask)) 
                || (Physics.Raycast(ShootRay, out shootHit, range, secondMask)) || (Physics.Raycast(ShootRay, out shootHit, range, thirdMask))) //맞혔으면 return
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>(); //맞힌 적의 체력 저장

            if (enemyHealth != null) //적에게 체력이 있으면, 다른 물체는 체력이 null
            {
                enemyHealth.TakeDamage(damagePerShot, shootHit.point);
            }
            gunLine.SetPosition(1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition(1, ShootRay.origin + ShootRay.direction * range);
        }
    }
}
