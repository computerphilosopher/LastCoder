﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController: MonoBehaviour
{
 private Inventory inventory;
    private bool isOpened;

    // Use this for initialization
    void Awake()
    {
        inventory = GetComponent<Inventory>();
        Debug.Log(inventory.name);
        isOpened = false;

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        { 
            Debug.Log("inventory");
            ToggleInventory();
        }

    }

    void ToggleInventory()
    {
        Debug.Log("inventory");

        if (isOpened)
        {
            inventory.closeInventory();
            isOpened = false;
        }

        else

        {
            inventory.openInventory();
            isOpened = true;
        }

    }

}
