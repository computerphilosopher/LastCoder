﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedObjController : MonoBehaviour
{
 private GameObject invenObj;
    private Canvas editorUI;
    private Canvas choiceUI;

    private GameObject codeEditor;
    private GccManager gccManager;

    private GameObject choiceEditor;
    private MultipleChoiceController multipleChoiceController;
    // private GameObject player;
    private GameObject player;
    private float distance;
    public int problemNumber;
    public int itemID;

    bool isLocked;

    private Component halo; //안 푼 문제가 있을시 오브젝트를 반짝이게 한다.

    DBParser parser;
    PlayerShooting playerShooting;

    // Use this for initialization
    void Awake()
    {

        try
        {
            codeEditor = GameObject.Find("TextEditor");
            editorUI = GameObject.Find("TextEditor").GetComponent<Canvas>();
            gccManager = codeEditor.GetComponent<GccManager>();
        }
        catch(System.NullReferenceException e)
        {
            Debug.Log("코딩 풀이 꺼진 상태" + e.Message);
 }
        

        try
        {
            choiceEditor = GameObject.Find("ChoiceUI");
            choiceUI = GameObject.Find("ChoiceUI").GetComponent<Canvas>();
            multipleChoiceController = choiceEditor.GetComponent<MultipleChoiceController>();
        }

        catch(System.NullReferenceException e)
        {

            Debug.Log("객관식 문제 꺼진 상태" + e.Message);
        }

        string dbPath = Application.dataPath + "/../Problem/";
        parser = new DBParser(problemNumber);
        player = GameObject.FindGameObjectWithTag("Player");
 
        if (player == null)
        {
            Debug.Log("null");
        }

        halo = GetComponent("Halo");

       
    }

    
    private void SetProblem()
    {
        if (parser.ProblemType.Equals("coding"))
        {
            gccManager.ProblemNumber = problemNumber;
            gccManager.itemID = itemID;
            gccManager.enabled = Toggle(gccManager.enabled);
            
        }

        else
        {
            multipleChoiceController.ProblemNumber = problemNumber;

            multipleChoiceController.itemID = itemID;
            multipleChoiceController.enabled=Toggle(multipleChoiceController.enabled);
        }
    }

    private Canvas GetUI()
    {
        if (parser.ProblemType.Equals("coding"))
        {
            return editorUI;
        }

        else
        {
            return choiceUI;
        }
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(gameObject.transform.position, player.transform.position);

        if (distance < 2)
        {
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                //player.SetActive(!문제object.activeInHierarchy);
                Canvas UI = GetUI();
                SetProblem();
                UI.enabled = Toggle(UI.enabled);

                if (UI.enabled)
                {

                }

            }
        }
        if (parser.IsSolved == true)
        {
            halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
        }
    }



    bool Toggle(bool b)
    {

        string dbPath = Application.dataPath + "/../Problem/";
        parser = new DBParser(problemNumber, dbPath);
        if (parser.IsSolved)
        {
            return false;
        }
        else
        {
            return !b;
        }
    }
}
