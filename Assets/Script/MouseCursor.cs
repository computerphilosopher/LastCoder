﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCursor : MonoBehaviour
{
    
    public Texture2D cursorTexture;  //CursorTexture For mousePointer
    public bool hotSpotIsCenter = true;  //Texture Cursor 중심을 마우스 포인터로 설정
    public Vector2 adjustHotSpot = Vector2.zero; //default mouse point

    public Vector2 hotSpot;
    bool isAiming;
    public void FixedUpdate()
    {
        isAiming = ThirdPersonUserControl.isAiming;
        
        //Call TargetCursor()
        StartCoroutine("MyCursor");
    }
    
    IEnumerator MyCursor()
    {
        //모든 렌더링이 완료 될 때까지 대기
        //완료 후 실행
        yield return new WaitForEndOfFrame();

        //텍스처의 중심을 마우스의 좌표로 사용하는 경우
        //텍스처의 폭과 높이의 1/2을 hot Spot 좌표로 설정
        if (hotSpotIsCenter)
        {
            hotSpot.x = cursorTexture.width / 2;
            hotSpot.y = cursorTexture.height / 2;
        }
        else
        {
            //default mouse pointer 좌표
            hotSpot = adjustHotSpot;
        }

        //사격이 가능한 상태일 때
        if (ThirdPersonUserControl.isAiming && !ThirdPersonUserControl.isCrouching && ThirdPersonCharacter.m_IsGrounded && !ThirdPersonCharacter.isAnim)
        {
            //Attack Cursor
            Cursor.SetCursor(cursorTexture, hotSpot, CursorMode.Auto); 
        }
        else
        {
            //default Cursor
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }

}
