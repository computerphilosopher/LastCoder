﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItem : MonoBehaviour
{

    public GameObject invenObj;

    private Inventory inventory;
    private bool isOpened;

    PlayerHealth playerHealth;

    // Use this for initialization
    void Awake()
    {
        inventory = invenObj.GetComponent<Inventory>();
        isOpened = false;

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            ToggleInventory();
        }

    }

    void OnEnable()
    {
        Inventory.ItemConsumed += OnConsumeItem;
    }

    void OnDisable()
    {
        Inventory.ItemConsumed -= OnConsumeItem;
    }

    public void OnConsumeItem(Item item)
    {
        for (int i = 0; i < item.itemAttributes.Count; i++)
        {
            if (item.itemAttributes[i].attributeName == "Health")
            {
                playerHealth.Heal(item.itemAttributes[i].attributeValue);
            }
        }
    }

    public void AddItem(int itemID)
    {
        inventory.addItemToInventory(itemID);
    }

    /* 인벤토리가 켜져 있으면 끄고 꺼져 있으면 켠다 */
    void ToggleInventory()
    {
        Debug.Log("inventory");

        if (isOpened)
        {
            inventory.closeInventory();
            isOpened = false;
        }

        else
        {
            inventory.openInventory();
            isOpened = true;
        }
    }

}
