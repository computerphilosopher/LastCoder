﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DicController : MonoBehaviour {

    private DicController dicController;
    private bool isOpened;
    public delegate void DicControllerOpend();
    public static event DicControllerOpend DicControllerOpen;
    public void closeDicController()
    {
        this.gameObject.SetActive(false);
        
    }

    public void openDicController()
    {
        this.gameObject.SetActive(true);
        if (DicControllerOpen != null)
            DicControllerOpen();
    }
    void Awake()
    {
        dicController = GetComponent<DicController>();
        Debug.Log(dicController.name);
        isOpened = false;

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            Debug.Log("dicController");
            ToggleDicController();
        }

    }

    void ToggleDicController()
    {
        Debug.Log("dicController");

        if (isOpened)
        {
            dicController.closeDicController();
            isOpened = false;
        }

        else

        {
            dicController.openDicController();
            isOpened = true;
        }

    }

}