﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{

    public float timeBetweenAttacks = 3f; // 각 공격 사이 시간
    public int attackDamage = 10; //attack damage
    public AudioClip enemyAttackClip;

    Animator anim;
    GameObject player;
    PlayerHealth playerHealth; //참조를 통해 적이 player에게 damage를 줄 수 있다
    EnemyHealth enemyHealth;
    AudioSource enemyAudio;

    bool playerInRange; //player를 공격 할 수 있는 거리
    float timer; //시간 동기화, 적이 느려지거나 빨라지지 않게 하기 위해

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player"); //player의 위치 확인
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent<Animator>();
        enemyAudio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other) //collider가 충돌하면 호출되는 함수, trigger이기 때문에 물리적 효과는 없다
    {
        if (other.gameObject == player) //충돌한 것이 player이면
        {
            playerInRange = true;
        }
    }

    void OnTriggerExit(Collider other) //collider에서 벗어나면 
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
    }

    void Update() //time 을 잰다
    {
        timer += Time.deltaTime;

        if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0) //공격할 수 있는 시간&공격범위내&체력 0 이상
        {
            Attack();
        }

        if (playerHealth.currentHealth <= 0) //player 체력이 0 이하면
        {
            anim.SetTrigger("PlayerDead");
        }
    }

    void Attack()
    {
        timer = 0f; //player를 공격 중이므로 timer reset
        if (playerHealth.currentHealth > 0) //player 살아있으면
        {
            anim.SetTrigger("PlayerAttack");
            enemyAudio.clip = enemyAttackClip; //
            enemyAudio.Play();
            playerHealth.TakeDamge(attackDamage); //player attack
        }
    }
}
