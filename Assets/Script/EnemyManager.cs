﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


/*
    *  좀비 파워 랭킹     (1~5 Level)
    * 1. Bald Zombie Guy     / 1
    * 2. Cute Zombie Girl    / 1
    * 3. Afro Zombie Girl    / 2
    * 4. Bearded Zombie Guy  / 3
    * 5. Nice Zombie Girl    / 3
    * 6. Fat Zombie Guy      / 4
    * 7. Stylish Zombie Guy  / 5
 */

public class EnemyManager : MonoBehaviour
{ 

    private PlayerHealth playerHealth; //player 체력이 0 이상이면 적 spawn
    public GameObject enemy; //생성되는 좀비
    public float spawnTime = 5f; // 좀비가 spawn되는 주기
    public int maxEnemyNum = 10;  //동시에 생성될 수 있는 최대 좀비 수 

    float timer;  //spawn 주기 check
    public float spawnRange = 10f;  // spawn되기 위한 player와 거리
    public Transform[] spawnPoints;  //spawnpoint 할당

    Transform player;
    float distance;  //player - spawnpoints 사이 거리
    bool enemySpawn; //spawn 가능한지

    GameObject[] zombie; //생성된 좀비들 counting

    AudioSource enemyAudio;  //boss zombie scream sound
    bool bossScream;  

  
    private void Awake()
    {
        enemyAudio = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        enemySpawn = true;
        bossScream = false;
    }


    private void Update()
    {
        timer += Time.deltaTime;

        //boss zombie 생성 시 Screamsound, anim, navmesh off - on
        if (bossScream)
        {
            BossScream(timer);
        }

        //spawnPoints[] 중 player가 거리 내 있으면 spawn
        for (int i = 0; i < spawnPoints.Length; i++) 
        {
            distance = Vector3.Distance(player.position, spawnPoints[i].position); //player와 spawnPoint 사이의 거리

            if (distance <= spawnRange && timer >= spawnTime) 
            {
                Spawn(i);
            }
        }
    }

    void Spawn(int spawnPoint)
    {
        print("spawn");
        timer = 0f; 

        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        if (enemy != null && enemySpawn)
        {
            //int spawnPointIndex = Random.Range(0, spawnPoints.Length);  //spawnpoint 중 랜덤으로 하나를 저장
            Instantiate(enemy, spawnPoints[spawnPoint].position, spawnPoints[spawnPoint].rotation); //zombie spawn
            CheckZombieNum();
        }
    }

    //scene에 동시에 spawn할 수 있는 좀비수 설정
    //zombie type마다 따로 좀비수 계산
    void CheckZombieNum()
    {
       
        if (enemy.name == "Fat Zombie Guy")
        {
            zombie = GameObject.FindGameObjectsWithTag("FatZombie");
        }
        else if (enemy.name == "Afro Zombie Girl")
        {
            zombie = GameObject.FindGameObjectsWithTag("AfroZombie");
        }
        else if (enemy.name == "Bald Zombie Guy")
        {
            zombie = GameObject.FindGameObjectsWithTag("BaldZombie");
        }
        else if (enemy.name == "Cute Zombie Girl")
        {
            zombie = GameObject.FindGameObjectsWithTag("CuteZombie");
        }
        else if (enemy.name == "Bearded Zombie Guy")
        {
            zombie = GameObject.FindGameObjectsWithTag("BeardedZombie");
        }
        else if (enemy.name == "Nice Zombie Girl")
        {
            zombie = GameObject.FindGameObjectsWithTag("NiceZombie");
        }
        //Boss Zombie
        else if (enemy.name == "Stylish Zombie Guy")
        {
            zombie = GameObject.FindGameObjectsWithTag("StylishZombie");
            enemyAudio.Play();
            bossScream = true;
        }

        if (zombie.Length < maxEnemyNum)
        {
            enemySpawn = true;
        }
        else
        {
            enemySpawn = false;
        }
    }

    //Scream시 잠시 멈춘다음에 이동함
    void BossScream(float time)
    {
        if(time <= 2.5f)
        {
            zombie[zombie.Length-1].GetComponent<NavMeshAgent>().enabled = false;
        }
        else
        {
            zombie[zombie.Length-1].GetComponent<NavMeshAgent>().enabled = true;
            bossScream = false;
        }
    }
}
