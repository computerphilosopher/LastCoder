﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Networking;


public class PlayerHealth : MonoBehaviour
{

    public int startingHealth = 100; //시작 체력
    public int currentHealth; //현재 체력
    public Slider healthSlider; //체력 바
    public Image damageImage; //피격 시 이미지
    public AudioClip deathClip; //죽을 시 사운드
    public float flashSpeed = 5; //damage 입을 시 이미지가 화면에 나타나는 시간
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f); //Red color, 0.1f : 10% 불투명함 설정
    public int currentScore = 0; 
    Animator anim;
    AudioSource playerAudio;
    ThirdPersonCharacter thridPersonCharacterMove; // player가 죽으면 돌아다니지 못하게 설정해야 함으로 참조
    PlayerShooting playerShooting;
    public string cc;
    bool isDead;
    bool damaged;
    public string UpdateUrl = "http://35.231.97.240:8080/UpgradeScore.jsp";
    //public string UpdateUrl = "http://localhost:8000/register/UpgradeScore.jsp";
    void Awake() //게임 시작과 함께 호출
    {
        anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        thridPersonCharacterMove = GetComponent<ThirdPersonCharacter>();
        playerShooting = GetComponentInChildren<PlayerShooting>();
        currentHealth = startingHealth;
    }

    void Update() //damage 입을 시 나타날 화면 설정
    {
        if (damaged)
        {
            damageImage.color = flashColour;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }

    public void TakeDamge(int amout) //damage 입을 시 값 설정
    {
        damaged = true;

        currentHealth -= amout;
        healthSlider.value = currentHealth; //현재 체력을 slider의 value로 parsing

        TakeDamagedAnim();
        playerAudio.Play(); //player hurt sound

        if (currentHealth <= 0 && !isDead) //player 체력이 100 이하면
        {
            Death();
        }
    }

    public void TakeDamagedAnim() //피격 시 3개의 anim 중 랜덤 실행
    {
        int n = UnityEngine.Random.Range(0, 3);

        if (n == 0)
        {
            anim.Play("Back_Impact[Pistol]", -1, 0f);
        }
        else if (n == 1)
        {
            anim.Play("Head_Impact[Pistol]", -1, 0f);
        }
        else
        {
            anim.Play("Stomach_Impact[Pistol]", -1, 0f);
        }
    }

    public void Heal(int amount)
    {
        if (currentHealth + amount > startingHealth)
        {
            currentHealth = startingHealth;
        }
        else
        {

            currentHealth += amount;
        }

        print("현재 체력:" + currentHealth);
        healthSlider.value = currentHealth; //현재 체력을 slider의 value로 parsing

    }

    public void Death()
    {
        print("death()실행");
        isDead = true;

        playerShooting.DisableEffects();

        anim.SetTrigger("Die"); //animator에서 Die trigget 설정해주기
        currentScore = ScoreManager.score; // 점수를 죽었을 때 받아와서 
        StartCoroutine(scoreUpdate());
        // 이어하기 or 처음부터 시작  

        playerAudio.clip = deathClip;
        playerAudio.Play();

        thridPersonCharacterMove.enabled = false; //player 이동 불가
        playerShooting.enabled = false; //player shooting 불가
    }

    IEnumerator scoreUpdate()
    {
        Debug.Log("앞에다가"+cc);
        cc = currentScore.ToString();
        Debug.Log("뒤에다가"+cc);
        WWWForm form = new WWWForm();
        form.AddField("id", starting.temiid);
        form.AddField("score", cc);
        UnityWebRequest www = UnityWebRequest.Post(UpdateUrl, form);
        yield return www.SendWebRequest();
        SceneManager.LoadScene(2);
    }
}
