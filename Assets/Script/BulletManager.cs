﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletManager : MonoBehaviour
{

    public static int maxBullet = 210; //최대 총알
    public static int magazineBulletSize = 30; //탄창 하나당 총알 수
    public static int currentBullet; //현재 총 보유 총알
    float timeBetweenReloadGun = 2.5f; //장전 시간

    public static int bullet; // 현재 탄창의 총알
    public static bool reloadGun; //장전(anim) 해야 하는가?
    public static bool ableShoot; //사격 가능한가?

    Text text; // "현재 탄창의 총알 / 총 총알"


    void Awake()
    {
        text = GetComponent<Text>();
        currentBullet = maxBullet;
        bullet = magazineBulletSize;
        reloadGun = false;
        ableShoot = true;

    }

    // Update is called once per frame
     void Update()
    {
     
        if (bullet == 0) //현재 탄창이 다 떨어지면
        {
            ableShoot = false;
            if (currentBullet != 0)  //모든 총알이 떨어졌는가
            {
                reloadGun = true; //장전 anim 실행
                StartCoroutine(GunReloadTime()); //장전 시 term 주기 위해
            }
            //else -> 사격 불가능
        }
        text.text = bullet + " / " + currentBullet;
    }

    IEnumerator GunReloadTime()
    {
        if (!ableShoot)
        {
            currentBullet = currentBullet - magazineBulletSize; 
            bullet = magazineBulletSize; // 탄창 채우기
        }
        yield return new WaitForSeconds(timeBetweenReloadGun);

        ableShoot = true;
    }

}