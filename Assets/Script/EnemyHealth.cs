﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHealth : MonoBehaviour
{

    public int startingHealth = 100; //시작 체력
    public int currentHealth;  //현재 체력
    public float sinkSpeed = 2.5f; //죽은 후 사라지는(가라앉는) 시간
    public int scoreValue = 30; //적 처치 시 얻는 점수
    public AudioClip deathClip;
    public GameObject bloodSplatter1; //좀비 피격 시 피 튀기는 particle
    
    AudioSource enemyAudio;
    Animator anim;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    EnemyMovement enemyMovement;

    bool isDead;
    bool isSinking;


    void Awake() // 참조 설정
    {
        anim = GetComponent<Animator>();
        enemyAudio = GetComponent<AudioSource>();
        hitParticles = GetComponentInChildren<ParticleSystem>(); //particle system을 찾아서 반환
        capsuleCollider = GetComponent<CapsuleCollider>();

        currentHealth = startingHealth; //현재 체력 설정
        enemyMovement = GetComponent<EnemyMovement>();
    }

    // Update is called once per frame
    void Update()
    { //죽은 후 가라앉을 것인지
        if (isSinking)
        {
           //transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }

    public void TakeDamagedAnim() //피격 시 2개의 anim 중 랜덤 실행
    {
        int n = Random.Range(0, 2);

        if (n == 0)
        {
            anim.Play("Damage", -1, 0f);
        }
        else
        {
            anim.Play("Damage2", -1, 0f);
        }
    }

    public void TakeDamage(int amout, Vector3 hitPoint) //타격 입을 시 작동 구현 함수
    {
        if (isDead)  //적이 죽었는 지
        {
            return;
        }

        /* -> 피격 시 잠시 그자리에 멈추도록, 아직 적용 안됨
        enemyMovement.enabled = false;
        float timer;
        for (timer = 0f; timer <= 4f; timer += Time.deltaTime)
        {
            print("timer : " + timer);
            if (timer > 3f)
            {
                print("true로");
                enemyMovement.enabled = true;
            }
        }
        */

        TakeDamagedAnim();

        enemyAudio.Play(); //공격 당하는 sound ,Mangled_Screams에서 SB_VoxPat_Monster Screech

        currentHealth -= amout; // 현재 체력에서 감소

        //hitParticles.transform.position = hitPoint; //survival shooter에 있는 particle, 먼지날리는 효과지만 여기엔 없음
        //hitParticles.Play();

        GameObject bloodSplatter = GameObject.Instantiate(bloodSplatter1, hitPoint, Quaternion.identity); //피 튀기게, instance화 해서 실행
        Destroy(bloodSplatter, 2f);

        if (currentHealth <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        isDead = true;

        capsuleCollider.isTrigger = true; // 적이 죽었을 때 장애물이 되지 않고 player가 이동 할 수 있도록

        anim.SetTrigger("Dead");

        enemyAudio.clip = deathClip; //MS_Mangled_Scream_12
        enemyAudio.Play();
        StartSinking(); //prefabs에 이 함수가 default로 없었기에 여기서 호출해줌
    }

    public void StartSinking()
    {
        GetComponent<NavMeshAgent>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        isSinking = true;
        ScoreManager.score += scoreValue; // 좀비 처치 후 점수 획득
        Destroy(gameObject, 2f);
    }
}
