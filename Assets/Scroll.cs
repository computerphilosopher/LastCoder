﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour {
   
    public GameObject CodingDic;
    public Canvas ch1book ,ch5book,ch6book,ch7book,ch8book;
    public Button lebutton, lebutton2, lebutton3, lebutton4, lebutton5;
  
    

// Use this for initialization
    void Start () {
        ch1book = ch1book.GetComponent<Canvas>();
        ch5book = ch5book.GetComponent<Canvas>();
        ch6book = ch6book.GetComponent<Canvas>();
        ch7book = ch7book.GetComponent<Canvas>();
        ch8book = ch8book.GetComponent<Canvas>();
        

        lebutton = lebutton.GetComponent<Button>();
        lebutton2 = lebutton2.GetComponent<Button>();
        lebutton3 = lebutton3.GetComponent<Button>();
        lebutton4 = lebutton4.GetComponent<Button>();
        lebutton5 = lebutton5.GetComponent<Button>();

        ch1book.enabled = false;
        ch5book.enabled = false;
        ch6book.enabled = false;
        ch7book.enabled = false;
        ch8book.enabled = false; 
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey("k"))
        {
            CodingDic.SetActive(true);
        }
		else if (Input.GetKey(KeyCode.Escape))
        {
            CodingDic.SetActive(false);
            ch1book.enabled = false;
            ch5book.enabled = false;
            ch6book.enabled = false;
            ch7book.enabled = false;
            ch8book.enabled = false;
        }
    }
	
   public void Clion()
    {
        ch1book.enabled = true;
        ch5book.enabled = false;
        ch6book.enabled = false;
        ch7book.enabled = false;
        ch8book.enabled = false;
    }
    public void Clion2()
    {
        ch5book.enabled = true;
        ch1book.enabled = false;
        ch6book.enabled = false;
        ch7book.enabled = false;
        ch8book.enabled = false;
    }

    public void Clion3()
    {
        ch5book.enabled = false;
        ch1book.enabled = false;
        ch7book.enabled = false;
        ch8book.enabled = false;
        ch6book.enabled = true;
    }
    public void Clion4()
    {
        ch5book.enabled = false;
        ch6book.enabled = false;
        ch1book.enabled = false;
        ch8book.enabled = false;
        ch7book.enabled = true;
    }
    public void Clion5()
    {
        ch5book.enabled = false;
        ch6book.enabled = false;
        ch7book.enabled = false;
        ch1book.enabled = false;
        ch8book.enabled = true;
    }
}
