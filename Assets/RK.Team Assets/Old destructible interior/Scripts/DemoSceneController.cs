﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoSceneController : MonoBehaviour
{

    #region Variables

    [Tooltip("Object spawn point in destruction demo scene.")]
    public Transform _objectSpawnPoint;

    [Tooltip("Default point for CameraOrbit script.")]
    public Transform _defaultOrbitPivot;

    [Tooltip("Link to the object with MouseOrbitImproved component.")]
    public MouseOrbitImproved _mouseOrbitController;

    [Tooltip("Default min distance from Camera to object.")]
    public float _defaultMinOrbit;

    [Tooltip("Default max distance from Camera to object.")]
    public float _defaultMaxOrbit;

    [Tooltip("List of prefabs for demo scene.")]
    public List<ObjectEntity> _objects = new List<ObjectEntity>();

    private GameObject _spawnedDestroyedObject;
    private GameObject _spawnedObject;
    private int _selectedObj;

    [Tooltip("UI element that controls force of explosion in Destruction demo.")]
    public Slider _explosionForceController;
    public Texture2D _canBeDestroyedCursor;

    public float _explosionForce;

    #endregion

    #region Functions

    public void Start()
    {
        _explosionForceController.value = _explosionForce;
        ChangeObject(0);
    }

    public void ChangeObject(int step)
    {
        _selectedObj = (int)Mathf.Repeat(_selectedObj += step, _objects.Count);

        if (_spawnedObject != null)
            Destroy(_spawnedObject);

        if (_spawnedDestroyedObject != null)
            Destroy(_spawnedDestroyedObject);

        _spawnedObject = (GameObject)Instantiate(_objects[_selectedObj]._objectBase, _objectSpawnPoint.position, Quaternion.identity, _objectSpawnPoint);
        _spawnedObject.transform.localRotation = Quaternion.identity;

        if (_objects[_selectedObj]._customOrbitPosition != null)
            _mouseOrbitController._orbitPivot = _objects[_selectedObj]._customOrbitPosition;
        else _mouseOrbitController._orbitPivot = _defaultOrbitPivot;

        if (_objects[_selectedObj]._customOrbitMin != 0)
            _mouseOrbitController.distanceMin = _objects[_selectedObj]._customOrbitMin;
        else _mouseOrbitController.distanceMin = _defaultMinOrbit;

        if (_objects[_selectedObj]._customOrbitMin != 0)
            _mouseOrbitController.distanceMax = _objects[_selectedObj]._customOrbitMax;
        else _mouseOrbitController.distanceMax = _defaultMaxOrbit;

        _mouseOrbitController.UpdateCameraPose();
    }

    public void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var _hit = new RaycastHit();

        if (!Physics.Raycast(ray, out _hit, 1000f))
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            return;
        }

        DestructibleObject _destructibleObject = _hit.collider.gameObject.GetComponent<DestructibleObject>();

        if (_destructibleObject == null)
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            return;
        }

        Cursor.SetCursor(_canBeDestroyedCursor, Vector2.zero, CursorMode.Auto);

        if (!Input.GetMouseButtonDown(0))
            return;

        _spawnedDestroyedObject = _destructibleObject.ObjectExplosionImpact(_hit.point, _explosionForce, 5f);
    }

    public void SetDestructionForce()
    {
        _explosionForce = _explosionForceController.value;
    }

    #endregion

}

#region Enums

public enum DestructionTypes
{
    NoDestruction = 0,
    Explosion = 1
}

public enum ObjectState
{
    Base = 0,
    Destroyed = 1
}

#endregion

#region Additional classes

[System.Serializable]
public class ObjectEntity
{
    public string _name;
    public GameObject _objectBase;

    public Transform _customOrbitPosition;
    public float _customOrbitMin = 0;
    public float _customOrbitMax = 0;
}

#endregion