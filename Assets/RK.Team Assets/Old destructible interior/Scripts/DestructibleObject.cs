﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))] // This script require Rigidbody
[RequireComponent(typeof(Collider))] // This script require Collider
public class DestructibleObject : MonoBehaviour
{
    public float _destroyMagnitude; // minimum collision speed for destroying the object. 
    public GameObject _destructiblePrefab; // prefab that is spawned when object is destroyed. 
    public AudioClip _destructionSound; // SFX that start playing if object is destroyed.

    private GameObject SpawnDestroyedObject() // This function spawn detroyed object in the same position and rotation as original object
    {
        if (_destructionSound != null)
        {
            var DestructionSoundSourceObject = new GameObject(); // Spawn empty object
            var DestructionSoundSourceComponent = DestructionSoundSourceObject.AddComponent<AudioSource>(); // Add audio source component to the created empty

            DestructionSoundSourceObject.transform.position = transform.position; // Move empty with audiosource to the object 
            DestructionSoundSourceComponent.PlayOneShot(_destructionSound); // Play destruction SFX

            Destroy(DestructionSoundSourceObject, _destructionSound.length); // Destroy empty with audiosource after playind clip
        }

        return (GameObject)Instantiate(_destructiblePrefab, transform.position, transform.rotation, transform.parent); // spawn destroyed version of the object
    }

    public void SimpleDestroyObject() // this function destroy object with minimum explosion force.
    {
        ObjectExplosionImpact(transform.position, 0f, 0f);
    }

    public GameObject ObjectExplosionImpact(Vector3 point, float force, float radius) // This function destroy object, spawn destroyed version and set explosion force to the parts of the destroyed object
    {
        gameObject.SetActive(false); // Disable not destroyed version of object
        var spawnedDestroyedObject = SpawnDestroyedObject(); // Spawn destroyed version of object

        var physicalObjects = spawnedDestroyedObject.GetComponentsInChildren<Rigidbody>(true); // Get all physics parts of destroyed object

        foreach (var physicObj in physicalObjects) // Set explosion force on each part of destroyed object
        {
            physicObj.useGravity = true;
            physicObj.isKinematic = false;

            physicObj.AddExplosionForce(force, point, radius);
        }

        return spawnedDestroyedObject;
    }

    public void OnCollisionEnter(Collision collision) // This function is called when object collide with other object
    {
        if (collision.impulse.magnitude >= _destroyMagnitude) // If collision speed is more then minimum then destroy the object.
        {
            SpawnDestroyedObject();
            Destroy(gameObject);
        }
    }
}
